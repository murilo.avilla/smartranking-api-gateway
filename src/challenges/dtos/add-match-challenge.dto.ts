import { IsNotEmpty } from 'class-validator';
import { Player } from 'src/players/interface/player.interface';
import { Result } from 'src/match/interfaces/match.interface';

export class AddMatchChallengeDTO {
  @IsNotEmpty()
  def: Player;

  @IsNotEmpty()
  result: Array<Result>;
}
