import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ClientProxySmartRanking } from 'src/proxymq/client-proxy';
import { CreateChallengeDTO } from './dtos/create-challenge.dto';
import { Player } from 'src/players/interface/player.interface';
import { firstValueFrom } from 'rxjs';
import { UpdateChallengeDTO } from './dtos/update-challenge.dto';
import { Challenge } from './interfaces/challenge.interface';
import { ChallengeStatus } from './interfaces/challenge-status.enum';
import { AddMatchChallengeDTO } from './dtos/add-match-challenge.dto';

@Controller('/api/v1/challenges')
export class ChallengesController {
  constructor(
    private readonly clientProxySmartRanking: ClientProxySmartRanking,
  ) {}

  private clientChallenge =
    this.clientProxySmartRanking.getClientProxyChallengeBackend();
  private clientAdminBackend =
    this.clientProxySmartRanking.getClientProxyAdminBackend();

  private logger = new Logger(ChallengesController.name);

  @Post()
  async createChallenge(@Body() createChallengeDTO: CreateChallengeDTO) {
    //verify requester in players
    const filterRequester = createChallengeDTO.players.filter(
      (player) => player._id == createChallengeDTO.requester,
    );

    if (filterRequester.length <= 0)
      throw new BadRequestException('Requester must be one of the players');

    await Promise.all(
      createChallengeDTO.players.map(async (player: Player) => {
        const playerFound = await firstValueFrom(
          this.clientAdminBackend.send('players-find', { _id: player._id }),
        );

        if (playerFound.length <= 0)
          throw new BadRequestException(
            `Player ${player._id} not found on database`,
          );

        //verify players in category
        if (player.category != createChallengeDTO.category)
          throw new BadRequestException(`${player._id} must be in category`);
      }),
    );

    this.clientChallenge.emit('challenge-create', createChallengeDTO);
  }

  @Get()
  async findChallenge(@Query('player') playerId: string) {
    const playerFound = await firstValueFrom(
      this.clientAdminBackend.send('players-find', { _id: playerId }),
    );

    if (playerFound.length <= 0)
      throw new BadRequestException('Player not found');

    return this.clientChallenge.send('challenge-find', { playerId });
  }

  @Put('/:id')
  async updateChallenge(
    @Param('id') _id: string,
    @Body() updateChallengeDTO: UpdateChallengeDTO,
  ) {
    const challengeFound: Challenge = await firstValueFrom(
      this.clientChallenge.send('challenge-find', { _id }),
    )[0];

    if (!challengeFound) throw new BadRequestException('Challenge not found');

    if (challengeFound.status != ChallengeStatus.PENDING)
      throw new BadRequestException('Challenge cannot be update');

    this.clientChallenge.emit('challenge-update', { _id, updateChallengeDTO });
  }

  @Delete('/:id')
  async deleteChallenge(@Param('id') _id: string) {
    const challengeFound: Challenge = await firstValueFrom(
      this.clientChallenge.send('challenge-find', { _id }),
    )[0];

    if (!challengeFound) throw new BadRequestException('Challenge not found');

    this.clientChallenge.emit('challenge-update', {
      _id,
      updateChallengeDTO: { status: ChallengeStatus.CANCELED },
    });
  }

  @Post('/:id/match')
  async addMatchChallenge(
    @Param('id') _id: string,
    @Body() addMatchChallengeDTO: AddMatchChallengeDTO,
  ) {
    const challengeFound: Challenge = await firstValueFrom(
      this.clientChallenge.send('challenge-find', { _id }),
    )[0];

    if (!challengeFound) throw new BadRequestException('Challenge not found');

    this.clientChallenge.emit('challenge-add-match', {
      _id,
      addMatchChallengeDTO,
    });
  }
}
