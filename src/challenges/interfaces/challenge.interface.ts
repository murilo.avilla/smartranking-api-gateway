import { Player } from 'src/players/interface/player.interface';
import { ChallengeStatus } from './challenge-status.enum';

export interface Challenge {
  dateTimeChallenge: Date;
  status: ChallengeStatus;
  dateTimeRequest: Date;
  dateTimeResponse: Date;
  requester: Player;
  category: string;
  match?: string;
  players: Player[];
}
