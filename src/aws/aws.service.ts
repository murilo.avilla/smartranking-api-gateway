import { Injectable, Logger } from '@nestjs/common';
import { S3 } from '@aws-sdk/client-s3';

@Injectable()
export class AwsService {
  private logger = new Logger(AwsService.name);

  public async fileUpload(file: any, id: string) {
    const s3 = new S3({ region: process.env.AWS_REGION });

    const fileExtension = file.originalname.split('.')[1];

    const urlKey = `${id}.${fileExtension}`;

    const params = {
      Body: file.buffer,
      Bucket: process.env.AWS_BUCKET,
      Key: urlKey,
    };
    const response = await s3.putObject(params).then(
      () => {
        return { url: this.generateUrl(urlKey) };
      },
      (err) => {
        this.logger.error(err);
      },
    );

    return response;
  }

  private generateUrl(key) {
    return `https://${process.env.AWS_BUCKET}.s3-sa-east-1.amazonaws.com/${key}`;
  }
}
