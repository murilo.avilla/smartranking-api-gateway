export interface Player {
  readonly _id: string;
  readonly phoneNumber: string;
  readonly email: string;
  readonly category: string;
  name: string;
  ranking: string;
  rankingPosition: number;
  urlPhoto: string;
}
