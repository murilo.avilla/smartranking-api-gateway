import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { CreatePlayerDTO } from './dto/create-player.dto';
import { Observable, firstValueFrom } from 'rxjs';
import { UpdatePlayerDTO } from './dto/update-player.dto';
import { ClientProxySmartRanking } from 'src/proxymq/client-proxy';
import { FileInterceptor } from '@nestjs/platform-express';
import { AwsService } from 'src/aws/aws.service';

@Controller('/api/v1')
export class PlayersController {
  private logger = new Logger(PlayersController.name);
  constructor(
    private readonly clientProxySmartRanking: ClientProxySmartRanking,
    private readonly awsService: AwsService,
  ) {}

  private clientAdminBackend =
    this.clientProxySmartRanking.getClientProxyAdminBackend();

  @Post('players')
  async createPlayer(@Body() createPlayerDTO: CreatePlayerDTO) {
    const category = await firstValueFrom(
      this.clientAdminBackend.send('categories-find', createPlayerDTO.category),
    );

    if (!category) throw new BadRequestException('Category Not Found');
    this.clientAdminBackend.emit('player-create', createPlayerDTO);
  }

  @Get('players')
  findPlayers(
    @Query('id') _id: string,
    @Query('categoryId') category: string,
  ): Observable<any> {
    return this.clientAdminBackend.send('players-find', {
      _id: _id ?? null,
      category: category ?? null,
    });
  }

  @Put('players/:_id')
  updatePlayer(
    @Param('_id') _id: string,
    @Body() updatePlayerDTO: UpdatePlayerDTO,
  ) {
    return this.clientAdminBackend.emit('player-update', {
      id: _id,
      player: updatePlayerDTO,
    });
  }

  @Delete('players/:_id')
  deletePlayer(@Param('_id') _id: string) {
    return this.clientAdminBackend.emit('player-delete', _id);
  }

  @Post('players/:_id/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Param('_id') _id: string) {
    const player: any[] = await firstValueFrom(
      this.clientAdminBackend.send('players-find', { _id }),
    );

    if (player.length <= 0) throw new BadRequestException('Player not found');

    const urlPhoto: any = await this.awsService.fileUpload(file, _id);

    const updatePlayerDTO: UpdatePlayerDTO = { urlPhoto: urlPhoto.url };

    await this.clientAdminBackend.emit('player-update', {
      id: _id,
      player: updatePlayerDTO,
    });

    return await firstValueFrom(
      this.clientAdminBackend.send('players-find', { _id }),
    );
  }
}
