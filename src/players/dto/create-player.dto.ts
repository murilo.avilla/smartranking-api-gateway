import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreatePlayerDTO {
  @IsNotEmpty()
  readonly phoneNumber: string;

  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  category: string;
}
