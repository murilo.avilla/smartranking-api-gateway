import { IsOptional, IsString } from 'class-validator';

export class UpdatePlayerDTO {
  @IsOptional()
  @IsString()
  category?: string;

  @IsOptional()
  @IsString()
  urlPhoto?: string;
}
