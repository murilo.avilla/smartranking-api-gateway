import { Module } from '@nestjs/common';
import { PlayersController } from './players.controller';
import { ProxymqModule } from 'src/proxymq/proxymq.module';
import { AwsModule } from 'src/aws/aws.module';

@Module({
  imports: [ProxymqModule, AwsModule],
  controllers: [PlayersController],
  providers: [],
})
export class PlayersModule {}
