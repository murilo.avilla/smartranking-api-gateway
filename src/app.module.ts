import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { CategoriesModule } from './categories/categories.module';
import { PlayersModule } from './players/players.module';
import { ProxymqModule } from './proxymq/proxymq.module';
import { AwsModule } from './aws/aws.module';
import { ChallengesModule } from './challenges/challenges.module';
import { MatchModule } from './match/match.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CategoriesModule,
    AppModule,
    PlayersModule,
    ProxymqModule,
    AwsModule,
    ChallengesModule,
    MatchModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
